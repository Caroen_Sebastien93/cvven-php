<?php

/**
 * Class Utilisateur hérite controller
 * @author: Riotor/Tounsi
 * @version 1.0.1
 */
class Utilisateur extends CI_Controller {

    /**
     * Constructeur 
     */
    public function __construct() {
        parent::__construct();

        $this->load->model('Utilisateur_modele');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

// PARTIE RESERVATION

    /*
        Fonction afficher permettant :
            -d'afficher les réservations en cours (pas encore acceptées) en fonction de l'adhérent connecté
            -d'afficher les réservations prises en compte (acceptées) en fonction de l'adhérent connecté.
    */

    public function afficher($numclient = 0) {

        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['login'] = $session_data['login'];
            $data['idadh'] = $session_data['idadh'];
            $data['num'] = $numclient;
            if ($numclient == 0) {
                show_404();
            }
            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('cancel', 'cancel', 'required');
            $this->form_validation->set_rules('suppr', 'suppr', 'required');

            if ($this->form_validation->run()) {
                $nb = $this->input->post('suppr');
                $this->Utilisateur_modele->deleteReservation($nb);
            }

            $data['titre'] = "Mes réservations";
            
            $data['ReservationEnCours'] = $this->Utilisateur_modele->getReservationEnCours($numclient);
            $data['ReservationValide'] = $this->Utilisateur_modele->getReservationValide($numclient);

            $this->load->view('templates/headerReserv', $data);
            $this->load->view('utilisateur/afficher', $data);
            $this->load->view('templates/footer', $data);
        }
    }


    /*
        Fonction creation permettant l'insertion des demandes de réservations dans la base de données
    */
    public function creation() {
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['login'] = $session_data['login'];
            $identifiant = $session_data['idadh'];
            $data['idadh'] = $identifiant;
            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('datedebut', 'datedebut', 'required');
            $this->form_validation->set_rules('menage', 'menage', '');
            $this->form_validation->set_rules('mobilite', 'mobilite', '');
            $this->form_validation->set_rules('nbpers', 'nbpers', 'required');
            $this->form_validation->set_rules('fk_categ', 'fk_categ', 'required');
            $this->form_validation->set_rules('restauration', 'restauration', 'required');

            if ($this->form_validation->run() === FALSE) {
                $data['titre'] = 'Réservations';
                $data['title'] = 'Faites vos réservations !';
                $data['test'] = '';

                $this->load->view('templates/headerlogin', $data);
                $this->load->view('utilisateur/CreationReservation', $data);
                $this->load->view('templates/footer', $data);
            } else {
                $test = $this->Utilisateur_modele->setReservation($identifiant);
                $data['titre'] = 'Réservations';
                $data['title'] = 'Faites vos réservations !';
                if ($test == 1) {
                    $test = "Demande de réservation prise en compte. Veuillez patienter
					jusqu'à confirmation de votre demande.";
                }
                $data['test'] = $test;
                $this->load->view('templates/headerlogin', $data);
                $this->load->view('utilisateur/CreationReservation');
                $this->load->view('templates/footer', $data);
            }
        }
    }

//FIN PARTIE RESERVATION






    /*
     * Fonction ajouter permettant de : 
     * - définir le titre de la page 
     * - de charger le header, le footer ainsi que la page ajouter.
     */

    public function ajouter() {
        $data['titre'] = ' Créer un utilisateur';

        $this->load->view('templates/header', $data);
        $this->load->view('utilisateur/ajouter', $data);
        $this->load->view('templates/footer', $data);
    }

    public function modifierMdp() {

        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['login'] = $session_data['login'];
            $data['titre'] = 'Modifier mot de passe';
            $this->load->view('templates/headerlogin', $data);
            $this->load->view('utilisateur/modifiermdp', $data);
            $this->load->view('templates/footer', $data);
        } else {
            //If no session, redirect to login page
            redirect('utilisateur/login', 'refresh');
        }
    }

    public function login() {
        $data['titre'] = 'Connexion à la page';

        $this->load->view('templates/header', $data);
        $this->load->view('utilisateur/login_view', $data);
        $this->load->view('templates/footer', $data);
    }

    public function accueil() {
        $data['titre'] = 'Bienvenue sur le site';

        $this->load->view('templates/header', $data);
        $this->load->view('utilisateur/accueil', $data);
        $this->load->view('templates/footer', $data);
    }

    public function modifier_mdp() {
        $mdp = $this->input->post('npassword');
        $mdp1 = $this->input->post('cpassword');

        $this->form_validation->set_rules('npassword', 'Nouveau mot de passe', 'required');
        $this->form_validation->set_rules('cpassword', 'Confirmez le mot de passe', 'required|matches[npassword]');

        if ($this->form_validation->run() == false) {
            $data['titre'] = 'Modifier votre mot de passe';
            $data['session'] = $this->session->userdata['logged_in'];
            redirect('utilisateur/index');
        } else {
            $session_data = $this->session->userdata('logged_in');
            //$data['login'] = $login['login'];
            $this->Utilisateur_modele->changer_mdp($session_data['login'], $mdp1);
            redirect('utilisateur/index');
        }
    }

    /*
     * Fonction ajouter_utilisateur permettant de :
     * de définir les différents champs du formulaire
     * de vérifier si le formulaire est bien rempli
     * redirige l'utilisateur en fonction de la validité du formulaire
     */

    public function ajouter_utilisateur() {


        $this->form_validation->set_rules('nomadh', 'nomadh', 'required');
        $this->form_validation->set_rules('prenomadh', 'prenomadh', 'required');
        $this->form_validation->set_rules('login', 'login', 'required');
        $this->form_validation->set_rules('mdp', 'mdp', 'required');
        $this->form_validation->set_rules('ville', 'ville', 'required');
        $this->form_validation->set_rules('codepostal', 'codepostal', 'required');
        $this->form_validation->set_rules('adresse', 'adresse', 'required');
        if ($this->form_validation->run() === FALSE) {

            redirect('utilisateur/ajouter');
        } else {

            $this->Utilisateur_modele->set_utilisateur();
            redirect('utilisateur/login');
        }
    }

    public function login_form() {
        $this->load->library('form_validation');

        $this->load->view('templates/header');


        $this->form_validation->set_rules('login', 'Login', 'required');
        $this->form_validation->set_rules('mdp', 'mdp', 'required|callback_check_database');

        if ($this->form_validation->run() == FALSE) {
            //Mauvaise combinaison login / mdp
            $this->load->view('utilisateur/login_view');
            $this->load->view('templates/footer');
        } else {
            //Connection effectuée
            redirect('utilisateur/index', 'refresh');
        }
    }

    function check_database($mdp) {
        $login = $this->input->post('login');
        $idadh = $this->input->post('idadh');

        $result = $this->Utilisateur_modele->login($login, $mdp);

        if ($result) {
            $sess_array = array();
            foreach ($result as $row) {
                $sess_array = array(
                    'idadh' => $row->idadh,
                    'login' => $row->login,
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', '<center><b>Login ou mot de passe incorrect!</b></center>');
            return false;
        }
    }

    function index() {
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['login'] = $session_data['login'];
            $data['idadh'] = $session_data['idadh'];
            $data['titre'] = 'Vous êtes connecté';
            $this->load->view('templates/headerlogin', $data);
            $this->load->view('utilisateur/home_view', $data);
            $this->load->view('templates/footer', $data);
        } else {
            //If no session, redirect to login page
            redirect('Utilisateur/login', 'refresh');
        }
    }

    function logout() {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('utilisateur/login', 'refresh');
    }

}