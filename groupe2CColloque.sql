--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.6

-- Started on 2017-04-21 11:27:13 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12403)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2178 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 181 (class 1259 OID 16435)
-- Name: idadh_seq; Type: SEQUENCE; Schema: public; Owner: groupe2c
--

CREATE SEQUENCE idadh_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483648
    CACHE 1;


ALTER TABLE idadh_seq OWNER TO groupe2c;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 182 (class 1259 OID 16437)
-- Name: adherent; Type: TABLE; Schema: public; Owner: groupe2c
--

CREATE TABLE adherent (
    idadh integer DEFAULT nextval('idadh_seq'::regclass) NOT NULL,
    nomadh character varying(50),
    prenomadh character varying(50),
    login character varying(50),
    mdp character varying(50),
    ville character varying(50),
    codepostal character(5),
    adresse character varying(50)
);


ALTER TABLE adherent OWNER TO groupe2c;

--
-- TOC entry 183 (class 1259 OID 16441)
-- Name: id_adh_seq; Type: SEQUENCE; Schema: public; Owner: groupe2c
--

CREATE SEQUENCE id_adh_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE id_adh_seq OWNER TO groupe2c;

--
-- TOC entry 184 (class 1259 OID 16443)
-- Name: logement; Type: TABLE; Schema: public; Owner: groupe2c
--

CREATE TABLE logement (
    idlogement integer NOT NULL,
    categ character varying(255),
    nbchambre integer,
    nblits integer
);


ALTER TABLE logement OWNER TO groupe2c;

--
-- TOC entry 2179 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN logement.categ; Type: COMMENT; Schema: public; Owner: groupe2c
--

COMMENT ON COLUMN logement.categ IS 'hohoho';


--
-- TOC entry 185 (class 1259 OID 16446)
-- Name: reservation; Type: TABLE; Schema: public; Owner: groupe2c
--

CREATE TABLE reservation (
    datedebut date,
    datefin date,
    menage boolean,
    etatreservation character varying(50),
    typereservation character varying(50),
    mobilitereduite boolean,
    fk_idlogement integer,
    fk_idadh integer,
    datereservation date,
    restauration character varying(62),
    nbpersonne integer,
    numreservation integer NOT NULL
);


ALTER TABLE reservation OWNER TO groupe2c;

--
-- TOC entry 186 (class 1259 OID 16449)
-- Name: reservation_numreservation_seq; Type: SEQUENCE; Schema: public; Owner: groupe2c
--

CREATE SEQUENCE reservation_numreservation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reservation_numreservation_seq OWNER TO groupe2c;

--
-- TOC entry 2180 (class 0 OID 0)
-- Dependencies: 186
-- Name: reservation_numreservation_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: groupe2c
--

ALTER SEQUENCE reservation_numreservation_seq OWNED BY reservation.numreservation;


--
-- TOC entry 2040 (class 2604 OID 16451)
-- Name: numreservation; Type: DEFAULT; Schema: public; Owner: groupe2c
--

ALTER TABLE ONLY reservation ALTER COLUMN numreservation SET DEFAULT nextval('reservation_numreservation_seq'::regclass);


--
-- TOC entry 2166 (class 0 OID 16437)
-- Dependencies: 182
-- Data for Name: adherent; Type: TABLE DATA; Schema: public; Owner: groupe2c
--

COPY adherent (idadh, nomadh, prenomadh, login, mdp, ville, codepostal, adresse) FROM stdin;
1	Riotor	Mathias	admin	admin	test	87845	test
2	test	test	test	test	test	test 	test
3	tounsi	muhammad	alqaida	test	syrie	270  	kabyle
\.


--
-- TOC entry 2181 (class 0 OID 0)
-- Dependencies: 183
-- Name: id_adh_seq; Type: SEQUENCE SET; Schema: public; Owner: groupe2c
--

SELECT pg_catalog.setval('id_adh_seq', 1, false);


--
-- TOC entry 2182 (class 0 OID 0)
-- Dependencies: 181
-- Name: idadh_seq; Type: SEQUENCE SET; Schema: public; Owner: groupe2c
--

SELECT pg_catalog.setval('idadh_seq', 35, true);


--
-- TOC entry 2168 (class 0 OID 16443)
-- Dependencies: 184
-- Data for Name: logement; Type: TABLE DATA; Schema: public; Owner: groupe2c
--

COPY logement (idlogement, categ, nbchambre, nblits) FROM stdin;
1	entrée, douche et wc, 2 chambres à 2 lits avec coin toilette et balcon.	2	4
2	entrée, douche et wc, 1 lit double.	1	1
3	3 lits séparés par une cloison mobile avec coin toilette, wc, douche.	1	3
4	4 lits séparés par une cloison mobile avec douche, wc et balcon.	2	4
5	logement adapté pour les personnes à mobilité réduite.	2	1
\.


--
-- TOC entry 2169 (class 0 OID 16446)
-- Dependencies: 185
-- Data for Name: reservation; Type: TABLE DATA; Schema: public; Owner: groupe2c
--

COPY reservation (datedebut, datefin, menage, etatreservation, typereservation, mobilitereduite, fk_idlogement, fk_idadh, datereservation, restauration, nbpersonne, numreservation) FROM stdin;
\.


--
-- TOC entry 2183 (class 0 OID 0)
-- Dependencies: 186
-- Name: reservation_numreservation_seq; Type: SEQUENCE SET; Schema: public; Owner: groupe2c
--

SELECT pg_catalog.setval('reservation_numreservation_seq', 48, true);


--
-- TOC entry 2044 (class 2606 OID 16453)
-- Name: Logement_pkey; Type: CONSTRAINT; Schema: public; Owner: groupe2c
--

ALTER TABLE ONLY logement
    ADD CONSTRAINT "Logement_pkey" PRIMARY KEY (idlogement);


--
-- TOC entry 2042 (class 2606 OID 16455)
-- Name: adherent_pkey; Type: CONSTRAINT; Schema: public; Owner: groupe2c
--

ALTER TABLE ONLY adherent
    ADD CONSTRAINT adherent_pkey PRIMARY KEY (idadh);


--
-- TOC entry 2046 (class 2606 OID 16457)
-- Name: logement_categ_key; Type: CONSTRAINT; Schema: public; Owner: groupe2c
--

ALTER TABLE ONLY logement
    ADD CONSTRAINT logement_categ_key UNIQUE (categ);


--
-- TOC entry 2048 (class 2606 OID 16459)
-- Name: reservation_pkey; Type: CONSTRAINT; Schema: public; Owner: groupe2c
--

ALTER TABLE ONLY reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (numreservation);


--
-- TOC entry 2049 (class 2606 OID 16460)
-- Name: reservation_fk_idadh_fkey; Type: FK CONSTRAINT; Schema: public; Owner: groupe2c
--

ALTER TABLE ONLY reservation
    ADD CONSTRAINT reservation_fk_idadh_fkey FOREIGN KEY (fk_idadh) REFERENCES adherent(idadh);


--
-- TOC entry 2050 (class 2606 OID 16465)
-- Name: reservation_fk_idlogement_fkey; Type: FK CONSTRAINT; Schema: public; Owner: groupe2c
--

ALTER TABLE ONLY reservation
    ADD CONSTRAINT reservation_fk_idlogement_fkey FOREIGN KEY (fk_idlogement) REFERENCES logement(idlogement);


--
-- TOC entry 2177 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-04-21 11:27:13 CEST

--
-- PostgreSQL database dump complete
--

